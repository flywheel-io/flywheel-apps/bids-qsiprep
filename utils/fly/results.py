"""Compress HTML files.
Main contributor: Luke Bloy 
Modified from bids-client/flywheel_bids/results/zip_htmls.py
To include any linked to files. assumed based on relative paths.
and to use zipfile directly.
"""

import logging
from pathlib import Path
from zipfile import ZIP_DEFLATED, ZipFile

from bs4 import BeautifulSoup
from flywheel_gear_toolkit.utils.zip_tools import zip_output

log = logging.getLogger(__name__)


def _get_linked_paths(input_html):
    """Search input html_path for hrefs"""
    input_html = Path(input_html)
    links = []
    with open(input_html, encoding="utf8") as fid_:
        soup = BeautifulSoup(fid_, "html.parser")
        for t in soup.find_all("a"):
            if t.get("href", None) and t["href"][0] == ".":
                links.append(t["href"])
    return links


def zip_derivatives(gear_options, analysis_output_dir):
    # this gear potentially creates 2 derivative folders
    #   qsiprep, qsirecon
    # that are zipped separately into
    #  <gear_name>_<project|subject|session label>_<analysis.id>_<qsiprep|qsirecon>.zip
    analysis_output_dir = Path(analysis_output_dir)
    for derivative in ["qsiprep", "qsirecon"]:
        derivative_dir = analysis_output_dir / derivative

        # rm streamlines ....
        if derivative == "qsirecon":
            for tck_file in derivative_dir.rglob("*.tck"):
                log.info(f"Removing Fiber tracks file {tck_file.relative_to(derivative_dir)}")
                tck_file.unlink()

        if (derivative_dir).exists():
            zip_file_name = (
                gear_options["output-dir"]
                / f"{gear_options['bids-app-binary']}_{gear_options['destination-id']}_{derivative}.zip"
            )
            zip_output(
                str(analysis_output_dir),
                derivative,
                str(zip_file_name),
                dry_run=False,
                exclude_files=None,
            )
            # zip any html.
            html_identifier = f'{derivative}_{gear_options["destination-id"]}'
            zip_htmls(gear_options["output-dir"], html_identifier, derivative_dir)


def zip_htmls(output_dir, destination_id, path):
    """Zip all .html files at the given path so they can be displayed
    on the Flywheel platform.
    Each html file must be converted into an archive individually:
      rename each to be "index.html", then create a zip archive from it.
    """
    path = Path(path)
    output_dir = Path(output_dir)

    log.info("Creating viewable archives for all html files")

    # create a html.zip archive for each file.
    for html_file in path.glob("*.html"):
        log.info(f"Creating viewable archive for {html_file.name}")

        # generate src files and zip names...
        src_files = [html_file]
        trg_files = ["index.html"]

        # add any linked to files.
        for linked_path in _get_linked_paths(html_file):
            this_file = html_file.parent / linked_path
            if this_file.exists():
                src_files.append(this_file)
                trg_files.append(linked_path)
            else:
                log.warning(f"Couldn't find {this_file}.")

        dest_zip = output_dir / f"{html_file.stem}_{destination_id}.html.zip"

        with ZipFile(dest_zip, "w", ZIP_DEFLATED) as outzip:
            log.debug(f"Creating {dest_zip}")
            for this_src, this_trg in zip(src_files, trg_files):
                log.debug(f"Zipping {this_src} -> {this_trg}")
                outzip.write(this_src, this_trg)
