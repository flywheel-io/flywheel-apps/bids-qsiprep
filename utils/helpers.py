"""Small utility functions for the gear."""
import json
import re
from os import PathLike


def check_eddy_config(eddy_config_path: PathLike, subject_label: str, session_label: str):
    """If eddy_config.json is provided, substitute subject and session labels."""
    search_dict = {"subject_label": subject_label, "session_label": session_label}
    with open(eddy_config_path, "r+") as f:
        contents = json.load(f)

        for k, v in search_dict.items():
            regex = re.compile(fr"\${{{k}}}", re.S)
            # contents['args']=regex.sub(lambda m: m.group().replace(v),contents['args'])
            contents["args"] = regex.sub(v, contents["args"])
            # make sure there are no doubled sub, ses strs
            regex_sub = re.compile("sub-sub", re.S)
            regex_ses = re.compile("ses-ses", re.S)
            contents["args"] = regex_sub.sub("sub", contents["args"])
            contents["args"] = regex_ses.sub("ses", contents["args"])
        f.seek(0)
        json.dump(contents, f, indent=4)
        f.truncate()
